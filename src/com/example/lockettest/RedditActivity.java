package com.example.lockettest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class RedditActivity extends Activity {
	RedditActivity currActivity = this;
	String outsideURL = "";
	ImageView i;
	TextView txtView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reddit);
		
		i = (ImageView) findViewById(R.id.imageView1);
		txtView = (TextView) findViewById(R.id.textView1);
		OnClickListener clickListener = new OnClickListener() {
		    public void onClick(View v) {
		    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(outsideURL));
				startActivity(browserIntent);
		    }
		};
		i.setOnClickListener(clickListener);
		
		
		new RetrieveRedditTask().execute("technology+worldnews+aww");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.reddit, menu);
		return true;
	}
	
	//Helper class for async calls
	private class RetrieveRedditTask extends AsyncTask<String, Void, String>
	{

		@Override
		protected String doInBackground(String... params) {
			String subreddit = params[0];
			String result = "Error";
			try {
				result = getFromReddit(subreddit);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
        protected void onPostExecute(String result) {
			String randomURL = "";
			String title = "";
			boolean isImage = false;
            try {
				JSONObject jObj = new JSONObject(result);
				JSONObject data = jObj.getJSONObject("data");
				JSONArray jArray = data.getJSONArray("children");
				JSONObject post = jArray.getJSONObject((int) (Math.random()*jArray.length()));
				JSONObject postData = post.getJSONObject("data");

				randomURL = postData.getString("url");
				
				while (true) {
					if(postData.getString("subreddit").equals("aww") && postData.getString("domain").equals("i.imgur.com") && !randomURL.substring(randomURL.length()-3, randomURL.length()).equals("gif")) {
						isImage = true;
						break;
					} else if(!postData.getString("url").substring(13, 19).equals("reddit") && (postData.getString("subreddit").equals("worldnews") || postData.getString("subreddit").equals("technology"))) {
						title = postData.getString("title");
						break;
					}
					post = jArray.getJSONObject((int) (Math.random()*jArray.length()));
					postData = post.getJSONObject("data");
				}
				randomURL = postData.getString("url");
				if(isImage) {
					outsideURL = "http://www.reddit.com"+postData.getString("permalink");
				} else {
					outsideURL = postData.getString("url");
					i.setBackgroundColor(Color.argb(255,0,191,255));
					txtView.setText(title);
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(isImage) {
		    	ImageLoader imgLoader = new ImageLoader(currActivity.getApplicationContext());
		        try {
		        	imgLoader.DisplayImage(randomURL, i);
		        } catch(Exception e) {
		        	onPostExecute(result);
		        	e.printStackTrace();
		        }
		    }
        }

	}
	
	public String getFromReddit(String subreddit) throws Exception
	{
	    String fullURLString = String.format(
	      "http://www.reddit.com/r/%s/.json?limit=200",
	      subreddit
	    );
	    URL url = new URL(fullURLString);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    BufferedReader br = new BufferedReader(
	      new InputStreamReader((conn.getInputStream()))
	    );
	    String output = "";
	    String out;
	    while ((out = br.readLine()) != null) {
	    	output += out+"\n";
	    }
	    conn.disconnect();
	    
	    return output;
	}
	
	public void destroy(){
		super.onDestroy();
	}
}
